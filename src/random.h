/*====================
  FUNCTION PROTOTYPES :
  ====================*/
  float     randomperc(); 
  float     get_beta();
  float get_delta();
  float   get_beta_rigid();
  double  noise();
  float     rndreal();
  int       small(float);
