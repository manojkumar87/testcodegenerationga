//user defined constants
#define BITS_PER_BYTE 8
#define UINTSIZE (BITS_PER_BYTE*sizeof(unsigned))
//#define INFINITY 1e7
#define EPSILON  1e-6
#define PI 3.1415927
#define MAXVECSIZE 30
#define MAXPOPSIZE 1000
#define TRUE 1
#define FALSE 0
#define BLX 0
#define SBX 1
#define ONESITE 1
#define UNIF 2
#define ONLINE 3
#define square(x)  ((x)*(x))

/*=================
TYPE DEFINTIONS :
=================*/
class indiv
            {
		public:
	  float x[MAXVECSIZE];     /* variables            */
               float obj;               /* objective fn. value  */
               float mod_obj;           /* modified objective   */
               unsigned *chrom;         /* chrosome string      */
               int parent1;
               int parent2;             /* s.no. of parents     */
               float cross_var;         /* cross over variable  */
            };
//typedef struct indiv INDIVIDUAL ;
typedef indiv *POPULATION ;        /* array of individuals */

/*====================
/FUNCTION PROTOTYPES :
====================*/
float   objective(float *);
void input_parameters();
void initialize();
void decode_string(indiv *);
void copy_individual(indiv *,indiv *);
void statistics(int );
void decodevalue(unsigned *,double *);
void generate_new_pop();
void binary_xover_new (unsigned * ,unsigned *  ,unsigned *  ,unsigned *  ,int  ,int );
void cross_over_1_site(int ,int ,int ,int );
void binmutation(unsigned *);
void mutation(indiv  *);
void free_all();
void select_memory();
void preselect_sr();
int sr_select();
void reset1();
float degraded_value(float ,float * ,POPULATION );
void select_memory_sr();
void select_free_sr() ;






