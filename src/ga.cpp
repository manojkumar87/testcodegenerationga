#include<iostream>
#include<fstream>
#include<iomanip>
#include<cstdio>
#include<vector>
#include<string>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<list>
#include"random.h"
#include"decl.h"
using namespace std;


void initreport (ofstream &);
void writechrom (unsigned *, ofstream &);
void report (ofstream &, int);
void write_in_file (ofstream &, int, float *, float, int);
void nomemory (string);
void error_ptr_null (string);

void allpaths (int src, int dest);
void input_app_parameters ();

/*Adjacency list is used to represent flow-graph*/
list < int >adj[200];
list < int >path;

/*Vector allp contains all paths between Start and End vertx*/
vector < string > allp;
int total_stmt_count;

//global variable 
int pop_size, gen_no, max_gen, best_ever_gen, num_var, num_discr_var, lchrom,
chromsize;
int no_xover, no_mutation;
int maxrun, run;
int REPORT, MINM = -1;
int *choices, nremain, tourneysize;
float *fraction;

float seed, basic_seed;
float p_xover, p_mutation, sigma_share;
float sum_obj, avg_obj, max_obj, min_obj;
float minx[MAXVECSIZE], maxx[MAXVECSIZE], x_lower[MAXVECSIZE],
x_upper[MAXVECSIZE];

POPULATION oldpop, newpop;  /* Old and New populations              */
indiv best_ever;    /* Best fit individual till current gen. */


int
main (int argc, char *argv[])
{
  ofstream fp_out ("realga.out", ios_base::out);
  int runno = 0, k;
  POPULATION temp;    /* A temporary pointer of population    */

  input_parameters ();

  select_memory ();
  select_memory_sr ();
  initreport (fp_out);
  for (run = 1; run <= maxrun; run++)
  {
    cout << "\nRun No. %d :  Wait Please .........", run;
    fp_out << "\nRun No. " << run;
    seed =
    basic_seed + (1.0 - basic_seed) * (float) (run - 1) / (float) maxrun;
    if (seed > 1.0)
     cout << "\n Warning !!! seed number exceeds 1.0";
   gen_no = 0;
   initialize ();
   statistics (gen_no);
   report (fp_out, 0);
   for (gen_no = 1; gen_no <= max_gen; gen_no++)
   {
     generate_new_pop ();

     temp = oldpop;
     oldpop = newpop;
     newpop = temp;

     statistics (gen_no);
     report (fp_out, gen_no);
  }     /* One GA run is over  */
     free_all ();
    }       /* for loop of run  */

     fp_out.close ();
     select_free_sr ();

     cout << "\n Results are stored in file 'realga.out' ";
     cout << "\n O.K Good bye !!!";
   }

//function to get input parameters from user
   void
   input_parameters ()
   {
    int k;
    char ans;


    cout << "\nHow many generations ? -------------";
    cin >> max_gen;
    cout << "\nPopulation Size ? ------------------ : ";
    cin >> pop_size;
    if (pop_size > MAXPOPSIZE)
    {
      cout << "\n Increase the value of MAXPOPSIZE in program";
      cout << "  and re-run the program";
      exit (-1);
    }
    cout << "\nCross Over Probability ? ( 0 to 1 )  : ";
    cin >> p_xover;
    cout << "\nMutation Probability ? ( 0 to 1 ) -- : ";
    cin >> p_mutation;
    cout << "\nNumber of variables (Maximum " << MAXVECSIZE << ") ---- : ";
    cin >> num_var;
    cout << "\n performing Binary crossover  ";
    num_discr_var = num_var;
    for (k = 0; k <= num_var - 1; k++)
    {
      cout << "\nLower and Upper bounds of x[" << k + 1 << "] ----- : ";
      cin >> x_lower[k] >> x_upper[k];
    }
    cout << "\n Total string length (each variable has equal string length)?";

    cin >> lchrom;
    cout << "\nSigma share value";
    cin >> sigma_share;

    cout << "\n Reports to be printed ? (y/n) ";
    do
    {
      ans = getchar ();
    }
    while (ans != 'y' && ans != 'n');
    if (ans == 'y')
      REPORT = TRUE;
    else
      REPORT = FALSE;
    cout << "\n How many runs ?" << endl;
    cin >> maxrun;
    tourneysize = 0;

    cout << "\n Give random seed (0 to 1.0)";
    cin >> basic_seed;
    input_app_parameters ();

  }

//Initialses zero'th generation and global parameters

  void
  initialize ()
  {
    float u;
    int k, k1, i, j, j1, stop;
    double temp[MAXVECSIZE], coef;
    unsigned mask = 1, nbytes;
    randomize (seed);

    oldpop = new indiv[pop_size];
    newpop = new indiv[pop_size];

    if (oldpop == NULL)
      nomemory ("oldpop in initialize()");
    if (newpop == NULL)
      nomemory ("newpop in initialize()");

    chromsize = (lchrom / UINTSIZE);

    if (lchrom % UINTSIZE)
      chromsize++;

    nbytes = chromsize * sizeof (unsigned);

    for (j = 0; j < pop_size; j++)
    {
      if ((oldpop[j].chrom = new unsigned[chromsize]) == NULL)
       nomemory ("oldpop chromosomes");

     if ((newpop[j].chrom = new unsigned[chromsize]) == NULL)
       nomemory ("newpop chromosomes");
   }

   if ((best_ever.chrom = new unsigned[chromsize]) == NULL)
    nomemory ("best_ever chromosomes");

  for (k = 0; k <= pop_size - 1; k++)
  {
    oldpop[k].parent1 = oldpop[k].parent2 = 0;
    for (j = num_discr_var; j <= num_var - 1; j++)
    {
     u = randomperc ();
     oldpop[k].x[j] = x_lower[j] * (1 - u) + x_upper[j] * u;

   }

   for (k1 = 0; k1 < chromsize; k1++)
   {
     oldpop[k].chrom[k1] = 0;
     if (k1 == (chromsize - 1))
       stop = lchrom - (k1 * UINTSIZE);
     else
       stop = UINTSIZE;

    /* A fair coin toss */

     for (j1 = 1; j1 <= stop; j1++)
     {

       if (flip (0.5))
        oldpop[k].chrom[k1] = oldpop[k].chrom[k1] | mask;
      if (j1 != stop)
        oldpop[k].chrom[k1] = oldpop[k].chrom[k1] << 1;

    }
  }
}
no_xover = no_mutation = 0;
copy_individual (&oldpop[0], &best_ever);
decode_string (&best_ever);
best_ever.obj = objective (best_ever.x);
}


//Decodes the string of the individual (if any) and puts the values in
//the array of floats.

void
decode_string (indiv * ptr_indiv)
{
  double *temp, coef;
  int j;

  if (ptr_indiv == NULL)
    error_ptr_null ("ptr_indiv in decode_string");
  temp = new double[num_discr_var];
  for (j = 0; j <= num_discr_var - 1; j++)
    temp[j] = 0.0;

  decodevalue (ptr_indiv->chrom, temp);
  coef = pow (2.0, (double) (lchrom / num_discr_var)) - 1.0;
  for (j = 0; j <= num_discr_var - 1; j++)
  {
    temp[j] = temp[j] / coef;
    ptr_indiv->x[j] = temp[j] * x_upper[j] + (1.0 - temp[j]) * x_lower[j];
  }
  delete[]temp;
}

//error handling functions
void
nomemory (string s)
{
  cout << "\nmalloc: out of memory making " << s << "!!\n";
  cout << "\n Program is halting .....";
  exit (-1);
}

void
error_ptr_null (string s)
{
  cout << "\n Error !! Pointer " << s << " found Null !";
  cout << "\n Program is halting .....";
  exit (-1);
}

//Copys contents of one individual into another.
void
copy_individual (indiv * indiv1, indiv * indiv2)
{
  int k;

  if (indiv1 == NULL)
    error_ptr_null ("indiv1 in copy_individual");
  if (indiv2 == NULL)
    error_ptr_null ("indiv2 in copy_individual");
  for (k = 0; k <= MAXVECSIZE - 1; k++)
    indiv2->x[k] = indiv1->x[k];
  indiv2->mod_obj = indiv1->mod_obj;
  indiv2->obj = indiv1->obj;
  indiv2->parent1 = indiv1->parent1;
  indiv2->parent2 = indiv1->parent2;
  indiv2->cross_var = indiv1->cross_var;
  for (k = 0; k <= chromsize; k++)
    indiv2->chrom[k] = indiv1->chrom[k];
}

//Calculates statistics of current generation :
void
statistics (int gen)
{
  indiv current_best;
  int k, j;
  float f;
  double pow (), bitpow, coef, temp[MAXVECSIZE];

  for (k = 0; k <= pop_size - 1; k++)
  {
    decode_string (&oldpop[k]);
    oldpop[k].obj = objective (oldpop[k].x);
    oldpop[k].mod_obj = degraded_value (oldpop[k].obj, oldpop[k].x, oldpop);

      //oldpop[k].mod_obj = oldpop[k].obj;
  }
  if (gen == 0)
  {
      //best_ever.mod_obj = best_ever.obj;
    best_ever.mod_obj = degraded_value (best_ever.obj, best_ever.x, oldpop);
  }

  current_best = oldpop[0];
  sum_obj = avg_obj = oldpop[0].obj;
  max_obj = min_obj = oldpop[0].obj;
  for (k = 0; k <= num_var - 1; k++)
    maxx[k] = minx[k] = oldpop[0].x[k];

  for (k = 1; k <= pop_size - 1; k++)
  {
    if (MINM * current_best.obj > MINM * oldpop[k].obj)
     current_best = oldpop[k];
   if (MINM * max_obj < MINM * oldpop[k].obj)
     max_obj = oldpop[k].obj;
   if (MINM * min_obj > MINM * oldpop[k].obj)
     min_obj = oldpop[k].obj;
   sum_obj += oldpop[k].obj;
   for (j = 0; j <= num_var - 1; j++)
   {
     if (MINM * maxx[j] < MINM * oldpop[k].x[j])
       maxx[j] = oldpop[k].x[j];
     if (MINM * minx[j] > MINM * oldpop[k].x[j])
       minx[j] = oldpop[k].x[j];
   }
 };
 avg_obj = sum_obj / pop_size;
 if (MINM * current_best.obj < MINM * best_ever.obj)
 {
  copy_individual (&current_best, &best_ever);
  best_ever_gen = gen;
}
}

//Decodes the value of a group of binary strings and puts the decoded
//values into an array 'value'.
void
decodevalue (unsigned *chrom, double *value)
{
  int k, j, stop, tp, bitpos, mask = 1, position, bits_per_var;
  double bitpow;

  if (chrom == NULL)
    error_ptr_null ("chrom in decodevalue");

  bits_per_var = lchrom / num_discr_var;
  for (k = 0; k < chromsize; k++)
  {
    if (k == (chromsize - 1))
     stop = lchrom - (k * UINTSIZE);
   else
     stop = UINTSIZE;
      /* loop thru bits in current byte */
   tp = chrom[k];
   for (j = 0; j < stop; j++)
   {
     bitpos = j + UINTSIZE * k;
    /* test for current bit 0 or 1 */
     if ((tp & mask) == 1)
     {
       position = bitpos / bits_per_var;
       bitpos -= position * bits_per_var;
       bitpow = pow (2.0, (double) (bits_per_var - bitpos - 1));
       value[position] += bitpow;
     }
     tp = tp >> 1;
   }
 }
}

float
degraded_value (float f, float *x, POPULATION pop)
{
  int k, j;
  float summation, dist, dist_sq;

  if (pop == NULL)
    error_ptr_null ("pop in degraded_value");
  if (x == NULL)
    error_ptr_null ("x in degraded_value");
  summation = 0;
  for (k = 0; k <= pop_size - 1; k++)
  {
    dist_sq = 0;
    for (j = 0; j <= num_var - 1; j++)
     dist_sq += (x[j] - pop[k].x[j]) * (x[j] - pop[k].x[j]);
   dist = sqrt (dist_sq);
   if (dist <= sigma_share)
     summation += 1.0 - dist / sigma_share;
 }
 if (summation < 1.0)
  summation = 1.0;
return (f / summation);
}



//generation of new population throught selection,crossovr and mutation
void
generate_new_pop ()
{
  int k, mate1, mate2;
  unsigned nbytes;
  indiv temp1, temp2;

  nbytes = chromsize * sizeof (unsigned);
  temp1.chrom = new unsigned[chromsize];
  temp2.chrom = new unsigned[chromsize];
  preselect_sr ();
  for (k = 0; k <= pop_size - 2; k += 2)
  {
      //performing selection
    mate1 = sr_select ();
    mate2 = sr_select ();

      //performing cross over one site
    cross_over_1_site (mate1, mate2, k, k + 1);

    mutation (&newpop[k]);
    mutation (&newpop[k + 1]);
    newpop[k].parent1 = newpop[k + 1].parent1 = mate1 + 1;
    newpop[k].parent2 = newpop[k + 1].parent2 = mate2 + 1;
  }
}

//Binary cross over routine.

void
binary_xover_new (unsigned *parent1, unsigned *parent2, unsigned *child1,
  unsigned *child2, int first, int second)
/* Cross 2 parent strings, place in 2 child strings */
{
  int j, jcross, k;
  unsigned mask, temp;

  if (parent1 == NULL)
    error_ptr_null ("parent1 in binary_xover");
  if (parent2 == NULL)
    error_ptr_null ("parent2 in binary_xover");
  if (child1 == NULL)
    error_ptr_null ("child1 in binary_xover");
  if (child2 == NULL)
    error_ptr_null ("child2 in binary_xover");

  //jcross = rnd(1 ,(lchrom - 1));/* Cross between 1 and l-1 */
  jcross =
  (int) ((oldpop[first].obj / (oldpop[first].obj + oldpop[second].obj)) *
    lchrom);

  for (k = 1; k <= chromsize; k++)
  {
    if (jcross >= (k * UINTSIZE))
    {
     child1[k - 1] = parent1[k - 1];
     child2[k - 1] = parent2[k - 1];
   }
   else if ((jcross < (k * UINTSIZE)) && (jcross > ((k - 1) * UINTSIZE)))
   {
     mask = 1;
     for (j = 1; j <= (jcross - 1 - ((k - 1) * UINTSIZE)); j++)
     {
       temp = 1;
       mask = mask << 1;
       mask = mask | temp;
     }
     child1[k - 1] =
     (parent1[k - 1] & mask) | (parent2[k - 1] & (~mask));
     child2[k - 1] =
     (parent1[k - 1] & (~mask)) | (parent2[k - 1] & mask);
   }
   else
   {
     child1[k - 1] = parent2[k - 1];
     child2[k - 1] = parent1[k - 1];
   }
 }
}


//cross over using strategy of 1 cross site with swapping .

void
cross_over_1_site (int first, int second, int childno1, int childno2)
{
  int k, site;
  float u;

  if (flip (p_xover))   /* Cross over has to be done */
  {
    no_xover++;
    binary_xover_new (oldpop[first].chrom, oldpop[second].chrom,
     newpop[childno1].chrom, newpop[childno2].chrom, first,
     second);


    }       /* Cross over done */

  else        /* Passing x-values straight */
    {
      for (k = 0; k <= chromsize; k++)
      {
       newpop[childno1].chrom[k] = oldpop[first].chrom[k];
       newpop[childno2].chrom[k] = oldpop[second].chrom[k];
     }
     for (k = 0; k <= num_var - 1; k++)
     {
       newpop[childno1].x[k] = oldpop[first].x[k];
       newpop[childno2].x[k] = oldpop[second].x[k];
     }
   }
 }




//Binary mutation routine 
 void
 binmutation (unsigned *child)
 {
  int j, k, stop;
  unsigned mask, temp = 1;

  if (child == NULL)
    error_ptr_null (" child in binmutation");
  for (k = 0; k < chromsize; k++)
  {
    mask = 0;
    if (k == (chromsize - 1))
     stop = lchrom - ((k - 1) * UINTSIZE);
   else
     stop = UINTSIZE;
   for (j = 0; j < stop; j++)
   {
     if (flip (p_mutation))
     {
       no_mutation++;
       mask = mask | (temp << j);
     }
   }
   child[k] = child[k] ^ mask;
 }
}

void
mutation (indiv * indiv)
{
  float distance1, distance2, x, delta, minu, maxu, u;
  int k, site;

  if (indiv == NULL)
    error_ptr_null ("indiv in mutation");

  binmutation (indiv->chrom);
}

//Reporting the user-specified parameters :
//fp is the file pointer to output file.
void
initreport (ofstream & fp)
{
  int k;
  fp << fixed;
  fp << "\n BINARY-CODED GA ";
  fp << "\n Tournament Selection Used (Size = " << tourneysize << ")";
  fp << "\n Crossover Strategy : 1 xsite with swapping";
  fp << "\n Mutation Strategy: Bit-wise Mutation";
  fp << "\n Variable Boundaries : ";
  fp << "\n Population size            : " << pop_size;
  fp << "\n Total no. of generations   : " << max_gen;
  fp << "\n Cross over probability     : " << setw (6) << setprecision (4) <<
  p_xover;
  fp << "\n Mutation probability       : " << setw (6) << setprecision (4) <<
  p_mutation;
  fp << "Sigma Share Value is  " << setw (6) << setprecision (4) <<
  sigma_share;
  fp << "\n String length              : " << lchrom;
  fp << "\n Number of variables        : " << num_var;
  fp << "\n Total Runs to be performed : " << maxrun;
  fp << "\n Lower and Upper bounds     :";
  fp.width (8);
  fp.precision (4);
  for (k = 0; k <= num_var - 1; k++)
    fp << "\n   " << setw (8) << setprecision (4) << x_lower[k] << "   <=   x"
  << k + 1 << "   <= " << setw (8) << setprecision (4) << x_upper[k];
  fp << "\n=================================================\n";
}

/*====================================================================
Writes a given string of 0's and 1's
puts a `-` between each substring (one substring for one variable)
Leftmost bit is most significant bit.
====================================================================*/
void
writechrom (unsigned *chrom, ofstream & fp)
{
  int j, k, stop, bits_per_var, count = 0;
  unsigned mask = 1, tmp;

  if (fp == NULL)
    error_ptr_null (" File fp in initreport");
  if (chrom == NULL)
    error_ptr_null ("chrom in writechrom");
  bits_per_var = lchrom / num_discr_var;
  for (k = 0; k < chromsize; k++)
  {
    tmp = chrom[k];
    if (k == (chromsize - 1))
     stop = lchrom - (k * UINTSIZE);
   else
     stop = UINTSIZE;

   for (j = 0; j < stop; j++)
   {
     if (tmp & mask)
       fp << "1";
     else
       fp << "0";
     count++;
     if (((count % bits_per_var) == 0) && (count < lchrom))
       fp << "-";
     tmp = tmp >> 1;
   }
 }
}

//Reporting the statistics of current population ( gen. no. 'num'):
 // fp is file pointer to output file.
void
report (ofstream & fp, int num)
{
  int k, j;
  FILE *fp_x, *fp_y;
  char string[30];


  if (fp == NULL)
    error_ptr_null (" file fp in report()");
  if (REPORT)
  {

    fp.width (3);
    fp << "\n========== Generation No. : " << num << " ===================";
    fp << "\n  No.      x       Fitness     Parents     random  ";
    fp << "\n===================================================";
    for (k = 0; k <= pop_size - 1; k++)
    {
     fp << "\n" << setw (3) << k +
     1 << ". [" << setw (8) << setprecision (5) << (int) oldpop[k].
       x[0] << "] ->";
for (j = 1; j <= num_var - 1; j++)
 fp << "\n      [" << setw (8) << setprecision (5) << (int)
   oldpop[k].x[j] << "] ->";
fp << "  " << setw (8) << setprecision (5) << oldpop[k].
obj << "  (" << setw (3) << oldpop[k].
 parent1 << " " << setw (3) << oldpop[k].
 parent2 << ")  " << setw (8) << setprecision (5) << oldpop[k].
cross_var;
if (num_discr_var >= 1)
{
 fp << "\n String = ";
 writechrom (oldpop[k].chrom, fp);
}
}
}
if ((REPORT) || (num == max_gen))
{

  fp << "\nMax = " << setw (8) << setprecision (5) << max_obj <<
  "  Min = " << setw (8) << setprecision (5) << min_obj << "   Avg = "
  << setw (8) << setprecision (5) << avg_obj;
  fp << "\nNo. of mutations = " << no_mutation << " ;  No. of x-overs = "
  << no_xover;

  fp << "\nBest ever = " << best_ever.
  x[0] << " -> fitness: " << best_ever.
  obj << " (from generation : " << best_ever_gen << ")";
  for (j = 1; j <= num_var - 1; j++)
   fp << "\n            " << best_ever.x[j];
 if (num_discr_var)
 {
   fp << "\nBest_ever String = ";
   writechrom (best_ever.chrom, fp);
 }

 fp << "\n\n";
}

}

//Releases the memory for all mallocs
void
free_all ()
{
  int i;

  for (i = 0; i < pop_size; i++)
  {
    delete oldpop[i].chrom;
    delete newpop[i].chrom;
  }
  delete[]oldpop;
  delete[]newpop;
  delete best_ever.chrom;
}


//Writes the results of GA run in the output file
void
write_in_file (ofstream & fp, int sno, float *x, float obj, int trials)
{
  int k;

  if (fp == NULL)
    error_ptr_null (" file fp in write_in_file");
  if (x == NULL)
    error_ptr_null (" x in write_in_file");

  fp << "\n" << setw (3) << sno << ".    " << setw (10) << setprecision (7) <<
  x[0] << "  " << setw (6) << trials << "  " << setw (10) <<
  setprecision (6) << minx[0] << " to " << maxx[0];
  for (k = 1; k <= num_var - 1; k++)
    fp << "\n       " << x[k] << "           " << minx[k] << " to " <<
  maxx[k];
  if (num_discr_var)
  {
    fp << "  ";
    writechrom (best_ever.chrom, fp);
  }
  fp << "\n          OBJECTIVE = " << setw (10) << setprecision (7) << obj <<
  "                ";
  fp << "\n       Random Seed Number : " << setw (6) << setprecision (4) <<
  seed << "                 ";
  cout << "  After " << trials / pop_size << " Generations : ";
  cout << "\n          OBJECTIVE = " << setw (10) << setprecision (7) << obj
  << "                ";
  cout << "\n Random Seed Number : " << setw (6) << setprecision (4) << seed
  << "                 ";
}



/* routines for for tournament selection :                         */

void
select_memory ()
{
  unsigned nbytes;

  if (tourneysize > pop_size)
  {
    cout << "FATAL: Tournament size (<<" << tourneysize <<
     "<<) > pop_size (" << pop_size << ")" << endl;
exit (-1);
}
}

void
select_memory_sr ()
{
  /* allocates auxiliary memory for stochastic remainder selection */

  unsigned nbytes;
  int j;

  nbytes = pop_size * sizeof (int);
  if ((choices = (int *) malloc (nbytes)) == NULL)
    nomemory ("choices");
  nbytes = pop_size * sizeof (float);
  if ((fraction = (float *) malloc (nbytes)) == NULL)
    nomemory ("fraction");
}

void
select_free_sr ()
{
  /* frees auxiliary memory for stochastic remainder selection */
  free (choices);
  free (fraction);
}


void
preselect_sr ()
/* preselection for stochastic remainder method */
{
  int j, jassign, k;
  float expected;

  if (avg_obj == 0)
  {
    for (j = 0; j < pop_size; j++)
     choices[j] = j;
 }
 else
 {
  j = 0;
  k = 0;

      /* Assign whole numbers */
  do
  {
   expected = ((oldpop[j].obj) / avg_obj);
   jassign = expected;
    /* note that expected is automatically truncated */
   fraction[j] = expected - jassign;
   while (jassign > 0)
   {
     jassign--;
     choices[k] = j;
     k++;
   }
   j++;
 }
 while (j < pop_size);

 j = 0;
      /* Assign fractional parts */
 while (k < pop_size)
 {
   if (j >= pop_size)
     j = 0;
   if (fraction[j] > 0.0)
   {
        /* A winner if true */
     if (flip (fraction[j]))
     {
      choices[k] = j;
      fraction[j] = fraction[j] - 1.0;
      k++;
    }
  }
  j++;
}
}
nremain = pop_size - 1;
}

int
sr_select ()
/* selection using remainder method */
{
  int jpick, slect;

  jpick = rnd (0, nremain);
  slect = choices[jpick];
  choices[jpick] = choices[nremain];
  nremain--;
  return (slect);
}




//OBJECTIVE FUNCTION  ( Supposed to be minimized) :
//Change it for different applications
float
objective (float *x)
{
  /* Program under test is instrumented so as to read input from input.txt instead of stdin.
     Program under test is also instrumented so that it stores path covered in path.txt and statements covered in atmt.txt.
   */
     int a, b, c, d, e;
     string path;
     int stmt_count = 0;
     a = (int) x[0];
     b = (int) x[1];
     c = (int) x[2];
     d = (int) x[3];
     e = (int) x[4];


  path = path + "0";    //instrumentation
  stmt_count += 2;    //instrumentation

  /*if(a<25)
     {
     path=path+"1";//instrumentation
     if(b<25)
     {
     path=path+"3";//instrumentation


     stmt_count+=25;//instrumentation
     }
     else
     {
     path=path+"4";//instrumentation


     stmt_count+=45;//instrumentation
     }
     path=path+"7";//instrumentation


     stmt_count+=70;//instrumentation
     }
     else
     {
     path=path+"2";
     if(c<25)
     {
     path=path+"5";//instrumentation


     stmt_count+=50;//instrumentation
     }
     else
     {
     path=path+"6";//instrumentation


     stmt_count+=20;//instrumentation
     }
     path=path+"8";//instrumentation


     stmt_count+=30;//instrumentation
     }

     path=path+"9";//end vertex


   */
     path = path + "0";
     stmt_count = stmt_count + 2;
     if (a < 30)
     {
      path = path + "1";
      if (b < 50)
      {
       path = path + "5";
    stmt_count += 30; //instrumentation
  }
  else
  {
   path = path + "6";
    stmt_count += 20; //instrumentation
  }
  path = path + "13";
      stmt_count += 20;   //instrumentation
    }
    else if (a > 30 && a < 50)
    {
      path = path + "2";
      if (c < 50)
      {

       path = path + "7";
    stmt_count += 90; //instrumentation
  }
  else
  {

   path = path + "8";
    stmt_count += 100;  //instrumentation
  }
  path = path + "14";
      stmt_count += 40;   //instrumentation
    }
    else if (a > 50 && a < 70)
    {
      path = path + "3";
      if (d < 50)
      {

       path = path + "9";
    stmt_count += 110;  //instrumentation
  }
  else
  {

   path = path + "10";
    stmt_count += 100;  //instrumentation

  }
  path = path + "15";
      stmt_count += 50;   //instrumentation
    }
    else
    {
      path = path + "4";
      if (e < 50)
      {

       path = path + "11";
    stmt_count += 10; //instrumentation
  }
  else
  {

   path = path + "12";
    stmt_count += 5;  //instrumentation
  }
  path = path + "16";
      stmt_count += 10;   //instrumentation
    }


  /*
     Check whether the path covered during execution is one of the previously enumerated paths
     i.e. whether the program was able to cover a successful path.
   */
     int feasible_path = 0;
     vector < string >::iterator result;
     result = find (allp.begin (), allp.end (), path);
  if (result != allp.end ())  /*Successful path covered */
     feasible_path = 1;

  /* Evaluate fitness of test-case */
     double acquired_fitness;
     acquired_fitness =
     (double) feasible_path + (double) stmt_count / (double) total_stmt_count;
     return acquired_fitness;

   }

   void
   input_app_parameters ()
   {
  /*
     Input Flow-Graph of the program under test and enumerate
     all paths all paths between start and end vertices.
   */

  int n;      /*Number of Vertices in Flow-Graph */
     cout << "Enter Number of Vertices (V) in Flow-Graph :";
     cin >> n;

     cout <<
     "Enter edge details for each vertex of Flow-Graph (starting from 0 to V-1) in the following format -"
     << endl;
     cout <<
     "<Number of directly connected vertices> <Directly connected vertices seperated by space>"
     << endl;
     for (int i = 0; i < n; i++)
     {
      int num, end;
      cin >> num;
      for (int j = 0; j < num; j++)
      {
       cin >> end;
       adj[i].push_back (end);
     }
    }       //end for

    cout << "Enter Start and End vertices :" << endl;
    int src, dest;
    cin >> src >> dest;

/*Enumerate all paths from start vertex to end vertex in the flow-graph of the program under test*/
    path.push_back (src);
    allpaths (src, dest);
/*At this point all paths between start and end vertices have been stored in a vector named allp*/


/*Get the number of total statements in the program under test*/
    cout << "Enter the number of total statements in the program under test :"
    << endl;
    cin >> total_stmt_count;



  }

  void
  allpaths (int src, int dest)
  {

/* Given adjacency list of the Flow-Graph function allpaths(src,dest) calculates all possible paths between src and dest */

    string str;
    if (src == dest)
    {
      list < int >::iterator it1;
      for (it1 = path.begin (); it1 != path.end (); it1++)
       str = str + (char) ((*it1) + '0');
     allp.push_back (str);
   }
   else
   {
      /*Traverse adjacency list of src */
    list < int >::iterator it;
    for (it = adj[src].begin (); it != adj[src].end (); it++)
    {
    /*Add *it to path */
     path.push_back (*it);
    /*Recursive call */
     allpaths (*it, dest);
    /*Remove *it from path */
     path.pop_back ();
   }
 }
}       //allpaths

/**     End of allpaths function     **/
