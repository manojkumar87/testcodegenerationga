
Generation of Test Cases using Genetic Algorithm
----------------------------------------------------------
Following approach is used for Test Case Generation  

* Start with a initial random population of Test Cases.
* Code Coverage is used as a metric for Objective Function
* Binary Genetic Algorithm is used to optimize Objective Function. 

